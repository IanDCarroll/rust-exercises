fn five() -> i32 {
    5
}

fn main() {
    let x = plus_one(five());

    println!("Johnny {} is alive!", x);
}

fn plus_one(x: i32) -> i32 {
  x + 1
}
