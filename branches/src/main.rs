fn main() {
    let number = 3;

    if { number < 5 } {
        statement({ number < 5 });
    } else { 
        statement(greater_than_five(number));
    }
}

fn greater_than_five(number: i32) -> bool {
    number < 5
}

fn statement(result: bool) {
    println!("condition was {}", result);
}

