# rust-exercises

Exercises from The Rust Programming Language by Steve Klabnik and Carol Nichols

### dependencies

Rust - download and install the latest version [here](https://www.rust-lang.org/en-US/install.html)

### run one of the project excercises

- `$ cd [whichever project dir]`
- `$ rustc main.rs`
- `$ ./main`

#### or use Cargo

- `$ cd [whichever project dir]`
- `$ cargo run`
