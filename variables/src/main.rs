const FIVE: u8 = 0b0000_0101;
const TEN: u8 =  1___0;

fn main() {
    let sum = FIVE + TEN;

    println!("The value of sum is: {}", sum);
}
